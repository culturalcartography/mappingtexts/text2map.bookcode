# text2map.bookcode

This is the repository of all the code from the book [*Mapping Texts: Computational Text Analysis for the Social Sciences*](https://www.textmapping.com/). Here are a few places you can get the book:

- [Oxford University Press](https://global.oup.com/academic/product/mapping-texts-9780197756881?cc=us&lang=en&#)
- [Powell's](https://www.powells.com/book/mapping-texts-9780197756881)
- [Bookshop.org](https://bookshop.org/p/books/mapping-texts-taylor/20115795?ean=9780197756881)
- [Thriftbooks](https://www.thriftbooks.com/w/mapping-texts_taylor/38852375/item/#edition=66760421&idiq=57084296)
- [Barnes \& Noble](https://www.barnesandnoble.com/w/mapping-texts-taylor/1143574271?ean=9780197756881)
- [Amazon](https://www.amazon.com/Mapping-Texts-Computational-Analysis-Sciences/dp/0197756883)

Note that the code presented here is updated periodically to fix any issues. Typically, these arise from updated dependencies. We explain any changes [here](https://www.textmapping.com/fixes/).

If you discover any issues or have suggestions, you can [open an issue directly on this repository](https://gitlab.com/culturalcartography/mappingtexts/text2map.bookcode/-/issues), submit the [feedback form on the website](https://textmapping.com/feedback/), or send us an email directly: maintainers [at] textmapping [dot] com.