## ----library_inductiveembeddings, echo = FALSE, -----------------------------


library(text2map)
library(text2map.pretrained)
library(text2vec)
library(stringi)
library(tidyverse)
library(textclean)
library(rtrek)



## ----wv_diachronic_startrek, ------------------------------------------------


df_trek <- st_transcripts()

df_trek$episode <- rownames(df_trek)

df_lines <- bind_rows(df_trek$text, .id = "episode") |>
            left_join(df_trek[-10], by = "episode")



## ----wv_prepare_series, -----------------------------------------------------


dia_df <- df_lines |>
    mutate(
        text = stri_trans_general(line,
               "Any-Latin; Latin-ASCII"),
        text = replace_contraction(text),
        text = tolower(text),
        text = gsub("[[:punct:]]+", " ", text),
        text = gsub("\\s+", " ", text)
    )



## ----wv_diachronic_list, ----------------------------------------------------


dia_df_list <- list()

for (i in unique(df_trek$series)) {
    dia_df_list[[i]] <- dia_df[dia_df$series == i, ]
}



## ----wv_mack_tcm_lapply, ----------------------------------------------------


make_tcm <- function(df) {
    words <- space_tokenizer(df$text)
    iterate <- itoken(words)
    voc <- create_vocabulary(iterate)
    vocab_vec <- vocab_vectorizer(voc)
    tcm <- create_tcm(iterate, vocab_vec, skip_grams_window = 5L)
    return(tcm)
}

dia_tcm <- lapply(dia_df_list, make_tcm)



## ----wv_macke_vectors_lapply, -----------------------------------------------


make_vectors <- function(tcm) {
    g_model <- GlobalVectors$new(
        rank = 300, x_max = 10,
        learning_rate = 0.05
    )
    vectors <- g_model$fit_transform(tcm, n_iter = 10)
    vectors <- vectors + t(g_model$components)
    return(vectors)
}

dia_vecs <- lapply(dia_tcm , make_vectors)



## ----wv_comparewords_align, -------------------------------------------------


shared_words <- Reduce(
    intersect,
    lapply(dia_vecs, rownames)
)
dia_vecs <- lapply(
    dia_vecs,
    function(i) i[shared_words, ]
)

aligned_vecs <- list()

for (i in seq_len(length(dia_vecs))) {
    if (i != 1) {
        aligned_vecs[[i]] <- find_transformation(
            dia_vecs[[i]],
            ref = dia_vecs[[1]],
            method = "align")
    } else {
        aligned_vecs[[i]] <- dia_vecs[[i]]
    }
}

names(aligned_vecs) <- names(dia_vecs)



## ----wv_diachronic_seriessim2, ----------------------------------------------


for (i in c("TAS", "TNG", "DS9", "VOY", "ENT")) {
    out <- sim2(
      aligned_vecs[["TOS"]]["human", , drop = FALSE],
      aligned_vecs[[i]]["human", , drop = FALSE],
      method = "cosine"
    )
    print(paste0(i, ": ", round(out, 4)))
}

## 
##     [1] "TAS: 0.2918"
##     [1] "TNG: 0.3051"
##     [1] "DS9: 0.2854"
##     [1] "VOY: 0.3633"
##     [1] "ENT: 0.3733"

## ----wv_diachronic_sim2b, ---------------------------------------------------


for (i in names(aligned_vecs)) {
    sim2(
        aligned_vecs[[i]],
        aligned_vecs[[i]]["human", , drop = FALSE]
    ) |>
        as.data.frame() |>
        filter(!rownames(aligned_vecs[[i]]) %in%
            c(get_stoplist("snowball2014"), "s")) |>
        arrange(desc(human)) |>
        head(n = 6) |>
        print()
}



## ----wmd_atnload, -----------------------------------------------------------


data("corpus_atn_immigr", package = "text2map.corpora")
data("contractions", package = "qdapDictionaries")

df_atn <- corpus_atn_immigr |>
  mutate(text = replace_non_ascii(text),
        text = replace_url(text, replacement = " "),
        text = replace_html(text, replacement = " "),
        text = replace_contraction(text,
            contraction.key = contractions,
            ignore.case = TRUE),
        text = str_replace_all(text, "[[:punct:]]", " "),
        text = replace_ordinal(text),
        text = str_replace_all(text, "[\\s]+", " "),
        text = tolower(text)
    )


## ----dtm_atn1, --------------------------------------------------------------


dtm_atn <- df_atn |>
    dtm_builder(text, doc_id) |>
    dtm_stopper(stop_list = get_stoplist("snowball2014"),
                stop_docprop = c(.01, Inf))



## ----wv_loadpretrained2, ----------------------------------------------------

# download the model once
download_pretrained("vecs_fasttext300_commoncrawl")
# load the model each session
data("vecs_fasttext300_commoncrawl", package ="text2map.pretrained")
wv <- vecs_fasttext300_commoncrawl # rename so it's tidier



## ----wv_comparevectors, -----------------------------------------------------


atn_vectors <- wv[rownames(wv) %in% colnames(dtm_atn), ]



## ----wv_anchors, ------------------------------------------------------------


anchors <- data.frame(
 add = c("immigrants", "immigration", "immigrant", "foreign",
          "foreigner", "outsider", "stranger", "alien", "foreigner",
          "alien", "immigrant", "foreign"),
 sub = c("citizens", "citizenship", "citizen", "domestic", "native",
          "insider", "local", "resident", "resident", "native",
          "local", "familiar")
)



## ----wv_getdirections, ------------------------------------------------------


cit_imm <- get_direction(anchors = anchors, wv = atn_vectors)



## ----citimmdirection, -------------------------------------------------------


sim_dir <- sim2(cit_imm, atn_vectors, method = "cosine")

df_dir <- data.frame(immigrants_pole = sim_dir["immigrants_pole", ],
                     terms = colnames(sim_dir)) |>
  mutate(cit_imm_label = ifelse(
            immigrants_pole >= 0,
            "Immigration", "Citizenship"),
        cit_imm = abs(immigrants_pole)
      )


## ----fig_citimm_w7_h6, ------------------------------------------------------


df_dir |>
  group_by(cit_imm_label) |>
  slice_max(cit_imm, n = 30) |>
  mutate(term = fct_reorder(terms, cit_imm)) |>
  ggplot(aes(term, cit_imm, fill = cit_imm_label, label = terms)) +
  geom_col() +
  guides(fill = "none") +
  labs(x = NULL, y = "Cosine Similarity to Pole") +
  coord_flip() +
  facet_wrap(~cit_imm_label, scale = "free")





## ----imm_censim, ------------------------------------------------------------


imm_cen <- get_centroid(anchors = anchors$add, wv = atn_vectors)

sims <- sim2(atn_vectors, imm_cen, method = "cosine")
names(head(sort(sims[,1], decreasing = TRUE)))


## ----library_wmdembeddings, echo = FALSE, -----------------------------------


library(text2map)
library(text2map.pretrained)
library(text2vec)
library(stringi)
library(tidyverse)
library(textclean)
library(rtrek)



## ----wv_loadittpr, ----------------------------------------------------------


data("corpus_ittpr", package = "text2map.corpora")
data("contractions", package = "qdapDictionaries")

df_ittpr <- corpus_ittpr |>
   mutate(text = replace_non_ascii(text),
          text = replace_url(text, replacement = " "),
          text = replace_html(text, replacement = " "),
          text = replace_contraction(text,
              contraction.key = contractions,
              ignore.case = TRUE),
          text = str_replace_all(text, "[[:punct:]]", " "),
          text = replace_ordinal(text),
          text = str_replace_all(text, "[\\s]+", " "),
          text = tolower(text)
    )



## ----dtmittpr, --------------------------------------------------------------


dtm_ittpr <- df_ittpr |>
    dtm_builder(text, doc_id) |>
    dtm_stopper(stop_list = get_stoplist("snowball2014"),
                stop_docprop = c(.01, Inf))


## ----dtm_atn1, --------------------------------------------------------------


dtm_atn <- df_atn |>
    dtm_builder(text, doc_id) |>
    dtm_stopper(stop_list = get_stoplist("snowball2014"),
                stop_docprop = c(.01, Inf))



## ----wv_prepdatasets, -------------------------------------------------------


df_atn_sub <- df_atn |>
    select(doc_id, lean, date, publication, text) |>
    filter(publication == "Talking Points Memo" |
           publication == "New York Times" |
           publication == "Buzzfeed News" |
           publication == "Breitbart" |
           publication == "Fox News" |
           publication == "National Review")

dtm_atn_sub <- df_atn_sub |>  dtm_builder(text, doc_id)



## ----wv_intersectvocab, -----------------------------------------------------


vocab <- intersect(rownames(atn_vectors), colnames(dtm_atn_sub))
vocab <- intersect(vocab, colnames(dtm_ittpr))

atn_vecs_sub <- atn_vectors[vocab, ]
dtm_atn_sub  <- dtm_atn_sub[, vocab]
dtm_ittpr    <- dtm_ittpr[, vocab]



## ----rwmd, ------------------------------------------------------------------


wmd_mod <- RWMD$new(x = dtm_ittpr, embeddings = atn_vecs_sub)
wmd_dis <- wmd_mod$sim2(x = dtm_atn_sub)



## ----rwmd_comparedists, -----------------------------------------------------


df_sim <- wmd_dis |>
    as.data.frame() |>
    rownames_to_column(var = "doc_id")

df_sim <- left_join(df_sim, df_atn_sub, by = "doc_id")



## ----rwmd_left_right, -------------------------------------------------------

r_idx <- df_ittpr$lean == "Right"
l_idx <- df_ittpr$lean == "Left"

df_atn_sub$wmd_r <- rowMeans(wmd_dis[, r_idx])
df_atn_sub$wmd_l <- rowMeans(wmd_dis[, l_idx])



## ----rwmd_leftrightreshape, -------------------------------------------------


df_plot <- df_atn_sub |>
    select(doc_id, lean, date, wmd_r, wmd_l) |>
    pivot_longer(cols = c("wmd_r", "wmd_l")) |>
    mutate(name = case_when(
           grepl("wmd_r", name) ~  "Right-Wing Press Releases",
           grepl("wmd_l", name) ~  "Left-Wing Press Releases")
        )



## ----fig_wmd_h5_w8, ---------------------------------------------------------


df_plot |>
  ggplot(aes(x = as.Date(date), y = value, color = name)) +
    geom_smooth(aes(linetype = name)) +
    scale_linetype_manual(values = c("twodash", "solid")) +
    labs(x = NULL, y = "Average Similarity to Press Releases") +
    guides(linetype = guide_legend(nrow = 2)) +
    theme(legend.position.inside = c(.65, .1)) +
    facet_wrap(~lean)


