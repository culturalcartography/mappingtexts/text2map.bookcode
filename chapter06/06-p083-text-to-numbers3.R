
## ----library_weighting, echo = FALSE, ---------------------------------------


library(tidyverse)
library(tidytext)
library(quanteda)
library(text2map)
library(ggpubr)



## ----relfreq, warning=FALSE, message=FALSE, ---------------------------------


data("corpus_europarl_subset", package = "text2map.corpora")

df_europarl <- corpus_europarl_subset |>
  filter(language == "English") |>
  rowid_to_column(var = "doc_id") |>
  mutate(
      text = tolower(text),
      text = gsub("[[:punct:]]", " ", text),
      text = gsub("[[:digit:]]+", " ", text),
      text = gsub("[[:space:]]+", " ", text)
  )


## ----relfreq_dtm, warning=FALSE, message=FALSE, -----------------------------


dtm <- df_europarl |> dtm_builder(text, doc_id)

doc_lengths <- rowSums(dtm)
dtm_rf <- dtm / doc_lengths
# inspect our DTM
dim(dtm_rf)


## ----relfreq_dtminspect, warning=FALSE, message=FALSE, ----------------------


dtm_rf_sub <- dtm_rf[seq_len(5), ]
# keep columns with non-zero values
dtm_rf_sub <- dtm_rf_sub[, colSums(dtm_rf_sub) != 0]
# inspect the first five words
round(dtm_rf_sub[, seq_len(5)], 3)



## ----relfreq_rowsums, warning=FALSE, message=FALSE, -------------------------

rowSums(dtm_rf_sub)





## ----calctf_idf, warning=FALSE, message=FALSE, ------------------------------


# we already have term frequencies
# assign it to a new object for clarity
tf <- dtm
# this calculates inverse-document frequencies
idf <- log10(nrow(dtm) / colSums(dtm != 0))
dtmtfidf <- tf * idf




## ----quanttf_idf, warning=FALSE, message=FALSE, -----------------------------


dtm_quanteda <- tokens(df_europarl$text) |> dfm()
dtmtfidf <- dfm_tfidf(dtm_quanteda)



## ----divide_loadsotus, ------------, echo=FALSE------------------------------


data("data_corpus_sotu", package = "quanteda.corpora")

text_sotu <- tidy(data_corpus_sotu) |>
rowid_to_column(var = "doc_id")

## ----divide_sotuswrangle, ------------, echo=FALSE---------------------------

text_sotu <- text_sotu |>
  mutate(
    text = tolower(text),
    text = gsub("[[:punct:]]", " ", text),
    text = gsub("[[:digit:]]+", " ", text),
    text = gsub("[[:space:]]+", " ", text)
  )

dtm <- text_sotu |> dtm_builder(text, doc_id)
dtm_rf <- dtm / rowSums(dtm)
idf <- log10(nrow(dtm_rf) / colSums(dtm_rf != 0))
dtmtfidf <- dtm_rf * idf



## ----nixondivide_sotus2, ------------, echo=TRUE-----------------------------


idx_nixon <- text_sotu$President == "Nixon"
idx_kennedy <- text_sotu$President == "Kennedy"

df_nixon <- data.frame(
        tf = colSums(dtm[idx_nixon, ]),
        rf = colSums(dtm_rf[idx_nixon, ]),
        tfidf = colSums(dtmtfidf[idx_nixon, ]),
        President = "Nixon",
        Term = colnames(dtm)
)

## ----kennedydivide_sotus2, ------------, echo=TRUE---------------------------

df_kennedy <- data.frame(
        tf = colSums(dtm[idx_kennedy, ]),
        rf = colSums(dtm_rf[idx_kennedy, ]),
        tfidf = colSums(dtmtfidf[idx_kennedy, ]),
        President = "Kennedy",
        Term = colnames(dtm)
)

## ----divid_sotus3, ------------, echo=TRUE-----------------------------------

df_plot <- rbind(df_nixon, df_kennedy) |>
  pivot_longer(cols = c("tf", "rf", "tfidf"),
              names_to = "Weight",
              values_to = "Value") |>
  group_by(President, Weight) |>
  slice_max(Value, n = 10)



## ----plot_nixon1, -----------------------------------------------------------


plot_nixon <- df_plot |>
    filter(President == "Nixon") |>
    mutate(Term = fct_reorder(Term, Value)) |>
    ggplot(aes(x = Term, y = Value, fill = Weight)) +
    geom_col() +
    facet_wrap(. ~ Weight, scales = "free", ncol = 1) +
    coord_flip() +
    labs(subtitle = "Top Terms in Nixon's Addresses")

## ----plot_kennedy2, ---------------------------------------------------------


plot_kennedy <- df_plot |>
    filter(President == "Kennedy") |>
    mutate(Term = fct_reorder(Term, Value)) |>
    ggplot(aes(x = Term, y = Value, fill = Weight)) +
    geom_col() +
    facet_wrap(. ~ Weight, scales = "free", ncol = 1) +
    coord_flip() +
    labs(subtitle = "Top Terms in Kennedy's Addresses")

## ----fig_weightcompare_w7_h8, -----------------------------------------------


ggarrange(plot_kennedy, plot_nixon, common.legend = TRUE)


