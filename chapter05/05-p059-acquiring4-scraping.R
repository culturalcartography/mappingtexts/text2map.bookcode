
## ----library_rvest, echo = FALSE, -------------------------------------------


library(tidyverse)
library(lubridate)
library(rvest)


## ----make_url2, ------------, echo = FALSE-----------------------------------


#THIS WONT PRINT IN BOOK

url <- "https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=&terms-0-field=title&classification-physics_archives=all&classification-statistics=y&classification-include_cross_list=include&date-filter_by=all_dates&date-year=&date-from_date=&date-to_date=&date-date_type=submitted_date&abstracts=show&size=50&order=-announced_date_first"



## ----paper_number, ----------------------------------------------------------


paper_number <- seq(0, 4950, by = 50)





## ----set_session, -----------------------------------------------------------


uast <- paste0(
    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 ",
    "(KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
)

sess <- session("https://arxiv.org/", httr::user_agent(uast))



## ----find_nodes, ------------------------------------------------------------


url |>
  session_jump_to(x = sess) |>
  html_elements("*") |> # asterisk matches any string
  unique() |>
  length()


## ----get_sessions1, ---------------------------------------------------------


# Create a list of all the URLs
url_list <- paste0(url, "&start=", paper_number)

# Create a list of webpages
page_list <- lapply(
    url_list,
    function(page) {
        Sys.sleep(5)
        return(session_jump_to(page, x = sess))
    }
)



## ----get_reviews1, ----------------------------------------------------------


paper_abstract <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".abstract-full") |>
            html_text()
    }
)



## ----check_reviews1, --------------------------------------------------------


length(paper_abstract) == 100
# remove extra white space with trimws
trimws(paper_abstract[[1]][1])



## ----get_info1, -------------------------------------------------------------


paper_id <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".is-inline-block > a") |>
            html_text()
    }
)



## ----get_info2, -------------------------------------------------------------


paper_title <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".is-5") |>
            html_text()
    }
)



## ----get_info3, -------------------------------------------------------------


paper_authors <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".authors") |>
            html_text()
    }
)



## ----get_info4, -------------------------------------------------------------


paper_subj <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".tags.is-inline-block") |>
            html_text()
    }
)



## ----get_info5, -------------------------------------------------------------


paper_date <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".mathjax+ .is-size-7") |>
            html_text()
    }
)



## ----create_dataset, --------------------------------------------------------


df_arxiv <- data.frame(
    id = unlist(paper_id),
    authors = unlist(paper_authors),
    title = unlist(paper_title),
    abstract = unlist(paper_abstract),
    tags = unlist(paper_subj),
    paper_date = unlist(paper_date),
    scrape_date = Sys.Date()
)

# look at our work
head(df_arxiv)



## ----axiv_cleantags1, -------------------------------------------------------


df_arxiv <- df_arxiv |>
    mutate(tags = str_squish(tags))

maximum <- max(str_count(df_arxiv$tags, "\\S+"))



## ----axiv_cleantags2, -------------------------------------------------------


df_arxiv <- df_arxiv |>
    separate(tags, paste0("tag_", seq_len(maximum)),
             extra = "merge", sep = "\\s+", fill = "right")



## ----axiv_cleanupdate, ------------------------------------------------------



df_arxiv$submit_date <- gsub("^.*Submitted\\s*|\\s*;.*$",
                             "", df_arxiv$paper_date)

df_arxiv$date_new <- parse_date_time(df_arxiv$submit_date,
                                     orders = c("dmy"))



## ----axiv_meltdf, -----------------------------------------------------------



melt_arxiv <- df_arxiv |>
    select(date_new, tag_1:tag_6) |>
    pivot_longer(!date_new)



## ----axiv_prepdf, -----------------------------------------------------------


melt_arxiv <- melt_arxiv |>
    filter(grepl("stat.", value, fixed = TRUE)) |>
    mutate(stats = recode(value,
        "stat.AP" = "Applications",
        "stat.CO" = "Computation",
        "stat.ME" = "Methodology",
        "stat.ML" = "Machine Learning",
        "stat.OT" = "Other Statistics")) |>
    group_by(stats, date_new) |>
    summarize(count = n())



## ----fig_arxivtime_w9_h3, ---------------------------------------------------


melt_arxiv |>
  ggplot(aes(x = date_new, y = count)) +
  geom_smooth() +
  labs(x = "Date", y = "Average",
      subtitle = "Papers per Stats Category") +
  facet_wrap(~stats, nrow = 1)


